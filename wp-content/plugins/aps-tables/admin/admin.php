<?php
/*
Plugin Name: APS Custom Table Plugin
Description: A plugin utilizing WP API for dynamic custom tables
Author: Elliot Sneeringer
Version: 0.1
*/

add_action('admin_menu', 'test_plugin_setup_menu');
 
function test_plugin_setup_menu(){
        add_menu_page( 'APS Tables', 'APS Tables', 'manage_options', 'aps-tables', 'test_init' );
}
 
function test_init(){
        echo "<h1>APS Tables Plugin Admin</h1>";
}
 
?>