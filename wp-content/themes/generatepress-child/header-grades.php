<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <main id="main">
 *
 * @package GeneratePress
 */
 
// No direct access, please
if ( ! defined( 'ABSPATH' ) ) exit;

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700,800" rel="stylesheet">
	<?php wp_head(); ?>
	<style>
		.draft-grades-year{
			color: #ff9e1b;
			text-align: left;
		}

		.inner-countdown-text{
			letter-spacing: 5px;
		}
	</style>
</head>

<body <?php generate_body_schema();?> <?php body_class(); ?>>
	<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'generatepress' ); ?>"><?php _e( 'Skip to content', 'generatepress' ); ?></a>
	<?php do_action( 'generate_before_header' ); ?>
	<?php do_action( 'generate_header' ); ?>
	<?php do_action( 'generate_after_header' ); ?>

	<!-- Header Image -->
	<!-- TO-DO: Depending on Page, change header Image -->
	<div class="header-image">
		<img class="shadow" src="/aps/wp-content/uploads/2017/11/Screen-Shot-2017-11-15-at-8.44.45-PM.png">
		<div class="overlay">
			<h3>2018 <span>NFL Draft</span></h3>
			<h1>Grades</h1>
			<p>Statement describing the following table of the mock drafts. This will be super informative and will not exceed three lines of text. There will be 37 Experts total.</p>
		</div>
	</div>

	<!--Countdown Clock -->
	<div class="countdown-box">	
		<div class="inner-countdown-box last-year">
			<div class="countdown-text">
				<div class="inner-countdown-text">
					<span class="draft-grades-year">2017</span> <br /><b>Draft of the Year</b>
				</div>
			</div>
			<div class="countdown">
				<div class="inner-countdown">
					<div class="name">
						CONGRATULATIONS <span>MIKE MAYOCK</span>
					</div>
					<span class="aps-score">80</span><span class="number-correct">11/32</span><span class="total-error">167</span><span class="avg-error">5.81</span><span class="diff-pts">107</span><span class="team-matching">8</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div id="page" class="hfeed site grid-container container grid-parent">
		<div id="content" class="site-content">
			<?php do_action( 'generate_inside_container' ); ?>