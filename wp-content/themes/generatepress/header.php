<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <main id="main">
 *
 * @package GeneratePress
 */
 
// No direct access, please
if ( ! defined( 'ABSPATH' ) ) exit;

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700,800" rel="stylesheet">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<style>
		body {
			background-color: #fff;
		}

		.clearfix {
			clear: both;
		}

		.countdown-box {		
		    width: 100%;
		    background-color: #000000;
		}

		.inner-countdown-box {
			padding: 15px;
		}

		.countdown-text {
			font-family: 'Montserrat', sans-serif;
		    font-size: 40px;
		    letter-spacing: 7px;
		    text-transform: uppercase;
		    color: #fff;
			width: 50%;
			float: left;
			text-align: right;
			font-weight: 300;
		}

		.inner-countdown-text {
			padding: 40px 25px 25px;
		}

		.countdown {
			float: left;
			width: 50%;
			text-align: left;
		}

		.inner-countdown {
			padding: 0px 25px 25px;
		}

		.days, .hours, .minutes, .seconds {
			color: #fff;
		    font-size: 70px;
		    border-right: 1px solid grey;
		    padding: 0px 65px 30px;
			position: relative;
		}

		.seconds {
			border-right: none;
		}

		.days:after, .hours:after, .minutes:after, .seconds:after {
			position: absolute;
		    bottom: -10px;
		    color: #F59020;
		    font-size: 18px;
		    left: 50%;
		    transform: translate(-50%, -50%);
			font-weight: bolder;
			letter-spacing: 0.5px;
		}

		.days:after {
			content: "Days";
		}
		.hours:after {
			content: "Hours";
		}
		.minutes:after {
			content: "Minutes";
		}
		.seconds:after {
			content: "Seconds";
		}

		.img-home {
			background-size: 100% 100%;
			transition: all 0.3s ease;
			color: #fff;
		}

		.flex-square-home-left {
			background-image: url('http://192.168.33.10/aps/wp-content/uploads/image_part_001.png');
		}
		.img-row a:hover .flex-square-home-left {
			background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('http://192.168.33.10/aps/wp-content/uploads/image_part_001.png');
			background-size: 110% 110%;
		}
		.flex-square-home-right {
			background-image: url('http://192.168.33.10/aps/wp-content/uploads/image_part_003.png');
		}
		.img-row a:hover .flex-square-home-right {
			background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('http://192.168.33.10/aps/wp-content/uploads/image_part_003.png');
			background-size: 110% 110%;
		}
		.flex-square-home-center {
			background-image: url('http://192.168.33.10/aps/wp-content/uploads/image_part_002.png');
		}
		.img-row a:hover .flex-square-home-center {
			background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('http://192.168.33.10/aps/wp-content/uploads/image_part_002.png');
			background-size: 110% 110%;
		}

		.img-row a {
			color: #fff;
			font-size: 25px;
			text-shadow: 1px 1px rgba(0,0,0,0.8);
			text-transform: uppercase;
		}

		.img-row a span {
			transition: all 0.3s ease;
			padding: 10px;
		}

		.img-row a:hover span {
			border: 2px solid #fff;
		}

		.uppercase {
			text-transform: uppercase;
		}

		.rise-fall {
			display: block;
			margin: 0 auto;
		}

		.home-text-content {
			text-align: center;
			display: block;
			margin: 0 auto;
			padding: 75px 150px;
		}

		.home-text-content h1 {
			font-weight: 700;
			font-size: 55px !important;
		}

		.home-text-content p {
			font-size: 25px;
		}

		.no-bullet {
			padding: 0;
			margin: 0;
		}

		.no-bullet li {
			list-style-type: none;
		}

		.inner-rise-fall {
			max-width: 600px;
		    display: block;
		    margin: 0 auto;
		}

		.inner-rise-fall .half {
			float: left;
			width: 50%;
		}

		.inner-rise-fall .title {
			font-family: proxima-nova, sans-serif;
			font-size: 18px;
			color: #ff9e1b;
			margin-bottom: 0px;
			text-align: left;
			letter-spacing: 1.5px;
			font-weight: 600;
		}

		.inner-rise-fall .inner-half {
			padding: 15px;
		}

		.inner-rise-fall .border {
			border-right: 2px solid #C1C2C3;
		}

		.inner-half.right {
			position: relative;
    		left: 90px;
		}

		.subscribe-box {
			background-color: #F6F6F7;
			text-align: center;
		}

		.inner-subscribe-box {
			padding: 50px;
			position: relative;
		} 

		.inner-subscribe-box .left {
			float: left;
			width: 30%;
			text-align: left;
		}

		.inner-subscribe-box .divider {
			position: absolute;
		    width: 3%;
		    height: 2px;
		    background-color: #ff9e1b;
		    margin-top: 7px;
		}

		.inner-subscribe-box .right {
			float: left;
			width: 70%;
		}

		.inner-subscribe-box .right .input {
			float: left;
			width: 80%;
		}

		.inner-subscribe-box .right .input input, .inner-subscribe-box .right .submit input {
			width: 100%;
		}

		.inner-subscribe-box .right .submit {
			float: left;
			width: 20%;
			text-align: left;
		}

		.inner-subscribe-box .right .submit-btn {
			background-color: #f79121;
		    text-transform: uppercase;
		    letter-spacing: 5px;
		}

		.inner-subscribe-box .left h3 {
			text-transform: uppercase;
			font-size: 30px;
			font-weight: 300;
		}

		.inner-subscribe-box .left p {
			margin: 0;
			width: 45%;
			font-size: 18px;
    		font-weight: 300;
		}

		.inner-subscribe-box .inner-form {
			padding: 30px 0;
		}

		footer {
			background-color: #000;
			text-align: center;
			color: #F0F0F0;
			font-family: proxima-nova, sans-serif;
		}

		.inner-footer {
			padding: 50px;
		}

		footer .left {
			text-align: left;
		}

		footer .left, footer .right {
			float: left;
			width: 50%;
		}

		footer .menu {
			width: 25%;
		}

		footer .menu ul {
			list-style-type: none;
    		margin: 0;
		}

		footer .menu ul li {
			width: 50%;
    		display: inline;
		}

		footer .menu ul li a {
			padding: 0 5px;
		    border-right: 1px solid #fff;
		    font-weight: 300;
		    font-size: 11px;
		    color: #F0F0F0;
		}

		footer .menu ul li:last-child a {
   	 		border-right: none;
		}

		.copyright {
			font-size: 11px;
			margin-top: 40px;
		}

		footer .logo {
			margin-bottom: 30px;
		}

		footer .social {
			text-transform: uppercase;
			color: #f79121;
			letter-spacing: 1px;
    		font-weight: 600;
    		font-size: 13px;
    		text-align: right;
		}

		footer .social a {
			color: #fff;
			transition: all 0.3s ease;
			font-size: 24px;
			padding: 0 10px;
		}

		footer .social a:hover {
			color: #f79121;
		}

		footer .input {
			float: left;
			width: 70%;
		}

		footer .input input, footer .submit input {
			width: 100%;
		}

		footer .submit {
			float: left;
			width: 30%;
		}

		footer .social span {
			position: relative;
			top: -5px;
			margin-right: 10px;
		}

		footer .email-input, footer .email-input:focus {
			background-color: #000;
		}

		footer .submit-btn {
			background-color: #000;
    		border: 1px solid #fff;
		}

		.email-footer {
			padding: 35px 0 15px;
		}

		.form-text {
			text-align: left;
			font-weight: 300;
		}

		.tablepress tfoot th, .tablepress thead th {
			background-color: #363637;
			color: #fff;
			text-transform: uppercase;
			text-align: center;
		}

		.tablepress .sorting:hover, .tablepress .sorting_asc, .tablepress .sorting_desc {
			background-color: #f79121;
		}

		.tablepress thead a, .tablepress thead a:visited {
			color: #fff;
		}

		.tablepress a:focus {
			color: #f79121;
		}

		.tablepress td a, .tablepress td a:focus {
			color: #000;
		}

		.tablepress .odd td {
		    background-color: #EDECED;
		}

		.tablepress tr {
			transition: all 0.3s ease;
		}

		.tablepress tr:hover, .tablepress tr:hover a {
			color: #f79121 !important;
		}

		@media all and (max-width: 1700px) {
			.countdown-text {
				font-size: 32px;
			}

			.inner-countdown-text {
			    padding: 30px 15px 15px;
			}

			.inner-countdown {
			    padding: 0px 15px 20px;
			}

			.days, .hours, .minutes, .seconds {
			    font-size: 50px;
			    padding: 0px 55px 30px;
			}
		}

		@media all and (max-width: 1390px) {
			.countdown-text {
				font-size: 24px;
			}

			.inner-countdown-text {
			    padding: 30px 15px 15px;
			}

			.inner-countdown {
			    padding: 0px 15px 20px;
			}

			.days, .hours, .minutes, .seconds {
			    font-size: 32px;
			    padding: 0px 50px 30px;
			}

			.inner-subscribe-box .left p {
			    width: 55%;
			}
		}

		@media all and (max-width: 1200px) {
			.countdown-text {
				font-size: 18px;
			}

			.inner-countdown-text {
			    padding: 30px 15px 15px;
			}

			.inner-countdown {
			    padding: 0px 15px 20px;
			}

			.days, .hours, .minutes, .seconds {
			    font-size: 24px;
			    padding: 0px 40px 20px;
			}

			.days:after, .hours:after, .minutes:after, .seconds:after {
				font-size: 12px;
			}

			.inner-subscribe-box .left p {
			    width: 65%;
			}

			.inner-subscribe-box .right .submit {
				width: 25%;
			}

			.inner-subscribe-box .right .input { 
				width: 75%;
			}

			footer .menu {
				width: 50%;
			}
		}

		@media all and (max-width: 992px) {
			.countdown-text {
				font-size: 18px;
				width: 100%;
				float: none;
			}

			.countdown {
				float: none;
				width: 100%;
			}

			.countdown-text, .countdown {
				text-align: center;
			}

			.inner-countdown-text {
			    padding: 15px;
			}

			.inner-subscribe-box, .inner-footer {
				padding: 40px;
			}

			.inner-subscribe-box .right .submit, .inner-subscribe-box .right .input {
				width: 100%;
				float: none;
				display: block;
				margin: 0 auto;
			}
		}

		@media all and (max-width: 768px) {
			.inner-subscribe-box .left, .inner-subscribe-box .right {
				float: none;
				display: block;
				margin: 0 auto;
				width: 100%;
			}

			.inner-subscribe-box .divider {
				width: 10%;
			}

			.inner-subscribe-box .left p {
				width: 100%;
			}

			footer .left, footer .right {
			    float: none;
			    width: 100%;
			    text-align: center;
			}

			footer .menu {
			    width: 100%;
			}

			footer .menu ul li a {
				font-size: 16px;
				padding: 0 5px;
			    border-right: none;
			    font-weight: 400;
			    font-size: 18px;
			    color: #F0F0F0;
			    background-color: #f79121;
			    padding: 10px;
			    margin: 5px;
			    display: block;

			}

			footer .menu ul li {
			    width: 100%;
			    display: block;
			}

			footer .social {
			    letter-spacing: 0px;
			    font-weight: 400;
			    font-size: 20px;
			    text-align: center;
			}

			footer .social span {
			    top: 0;
			}
		}

		@media all and (max-width: 650px) {
			.flex-square-home-left, .flex-square-home-center, .flex-square-home-right {
				float: none;
				display: block;
				margin: 0 auto;
				width: 100%;
				max-width: 650px;
				padding-top: 100px;
				padding-bottom: 100px;
			}

			.home-text-content {
				padding: 50px 100px;
			}

			.inner-rise-fall {
				max-width: 450px;
			}

			.inner-half.right {
				left: 60px;
			}

			.inner-subscribe-box, .inner-footer {
				padding: 30px;
			}
		}

		@media all and (max-width: 480px) {
			.home-text-content {
				padding: 25px 50px;
			}

			.inner-half.right {
				left: 40px;
			}

			.inner-subscribe-box, .inner-footer {
				padding: 20px;
			}
		}
	</style>
</head>

<body <?php generate_body_schema();?> <?php body_class(); ?>>
	<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'generatepress' ); ?>"><?php _e( 'Skip to content', 'generatepress' ); ?></a>
	<?php do_action( 'generate_before_header' ); ?>
	<?php do_action( 'generate_header' ); ?>
	<?php do_action( 'generate_after_header' ); ?>

	<!-- Header Image -->
	<!-- TO-DO: Depending on Page, change header Image -->
	<div class="header-image">
		<img src="/aps/wp-content/uploads/2017/11/Screen-Shot-2017-11-15-at-8.44.45-PM.png" alt="Smiley face">
	</div>

	<!--Countdown Clock -->
	<div class="countdown-box">	
		<div class="inner-countdown-box">
			<div class="countdown-text">
				<div class="inner-countdown-text">
					<b>2018 NFL Draft</b> Countdown
				</div>
			</div>
			<div class="countdown">
				<div class="inner-countdown">
					<span id="days" class="days">92</span><span id="hours" class="hours">21</span><span id="minutes" class="minutes">11</span><span id="seconds" class="seconds">41</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	
<script>
// Set the date we're counting down to
var countDownDate = new Date("Apr 5, 2018 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    //document.getElementById("demo").innerHTML = days + "d " + hours + "h "
    //+ minutes + "m " + seconds + "s ";
    
    document.getElementById("days").innerHTML = days;
    document.getElementById("hours").innerHTML = hours;
    document.getElementById("minutes").innerHTML = minutes;
    document.getElementById("seconds").innerHTML = seconds;
    
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
    }
}, 1000);
</script>

	<div id="page" class="hfeed site grid-container container grid-parent">
		<div id="content" class="site-content">
			<?php do_action( 'generate_inside_container' ); ?>