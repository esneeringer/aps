<?php
add_action( 'wp_enqueue_scripts', 'add_assets' );
function add_assets() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'feather-light-style', get_template_directory_uri() . '/inc/featherlight-1.7.9/src/featherlight.css' );
    wp_enqueue_style( 'feather-light-style-gallery', get_template_directory_uri() . '/inc/featherlight-1.7.9/src/featherlight.gallery.css' );
    wp_enqueue_script( 'aps-js', get_stylesheet_directory_uri() .'/js/aps.js');
    wp_enqueue_script( 'feather-light', get_stylesheet_directory_uri() . '/inc/featherlight-1.7.9/src/featherlight.js');
    wp_enqueue_script( 'feather-light-gallery', get_stylesheet_directory_uri() . '/inc/featherlight-1.7.9/src/featherlight.gallery.js');
}

// Add Shortcode
function generate_scorecard( $atts ) {

	// Attributes
	$atts = shortcode_atts(
		array(
			'id' => '',
		),
		$atts,
		'scorecard'
	);

	return '<div class="scorecard">
		<div class="inner-scorecard">
			<div class="section">
				<div class="inner-section">
					<div class="left">
						<div class="inner-left">
							<div class="key">'.do_shortcode("[table-cell id=". esc_attr($atts["id"]) ." cell=A1 /]").'</div>
							<div class="value">'.do_shortcode("[table-cell id=". esc_attr($atts["id"]) ." cell=B1 /]").'</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="right">
						<div class="inner-right">
							<div class="description">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=C1 /]").'</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="section">
				<div class="inner-section">
					<div class="left">
						<div class="inner-left">
							<div class="key">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=A2 /]").'</div>
							<div class="value">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=B2 /]").'</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="right">
						<div class="inner-right">
							<div class="description">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=C2 /]").'</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="section">
				<div class="inner-section">
					<div class="left">
						<div class="inner-left">
							<div class="key">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=A3 /]").'</div>
							<div class="value">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=B3 /]").'</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="right">
						<div class="inner-right">
							<div class="description">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=C3 /]").'</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="section">
				<div class="inner-section">
					<div class="left">
						<div class="inner-left">
							<div class="key">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=A4 /]").'</div>
							<div class="value">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=B4 /]").'</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="right">
						<div class="inner-right">
							<div class="description">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=C4 /]").'</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="section">
				<div class="inner-section">
					<div class="left">
						<div class="inner-left">
							<div class="key">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=A5 /]").'</div>
							<div class="value">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=B5 /]").'</div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="right">
						<div class="inner-right">
							<div class="description">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=C5 /]").'</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="section">
				<div class="inner-section">
					<div class="left">
						<div class="inner-left">
							<div class="key"><i class="fa fa-angle-right" aria-hidden="true"></i> '.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=A6 /]").'</div>
							<div class="value"><b>'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=B6 /]").'</b></div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="right">
						<div class="inner-right">
							<div class="description">'.do_shortcode("[table-cell id=".esc_attr($atts["id"])." cell=C6 /]").'</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>';

}
add_shortcode( 'scorecard', 'generate_scorecard' );

?>