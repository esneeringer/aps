<?php
/**
 * Template Name: APS Grades Main Template
 *
 * @package GeneratePress
 */

// No direct access, please
if ( ! defined( 'ABSPATH' ) ) exit;
get_header('grades'); ?>
<style>
    .tablepress-id-3 th,
    .tablepress-id-3 td {
        border: 1px solid #cccccc;
    }

    .tablepress-id-3 .column-1{
        width: 5%;
        border-left: none;
        text-align: center;
    }

    .tablepress-id-3 .column-2{
        width: 15%;
        padding-left: 50px;
    }

    .tablepress-id-3 .column-3{
        width: 10%;
        padding-left: 50px;
    }

    .tablepress-id-3 .column-4{
        width: 5%;
        padding-left: 50px;
        text-align: center;
        border-right:none;
    }



    #year-select{
        float:right;
        margin-bottom: 1%;
        background-color: #ff9e1b;
        color: white;
            -webkit-appearance: none;
            -webkit-border-radius: 0px;
        padding: 0.5%;
        font-size: small;
        letter-spacing: 2px;
        border: none;
    }

</style>

<select id="year-select">
  <option value="2017">Year: 2017 &#8744;</option>
  <option value="2016">Year: 2016 &#8744;</option>
</select>

<div id="primary" <?php generate_content_class();?>>
    <main id="main" <?php generate_main_class(); ?>>
        <?php do_action('generate_before_main_content'); ?>
        <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'content', 'page' ); ?>

            <?php
            // If comments are open or we have at least one comment, load up the comment template
            if ( comments_open() || '0' != get_comments_number() ) : ?>
                <div class="comments-area">
                    <?php comments_template(); ?>
                </div>
            <?php endif; ?>

        <?php endwhile; // end of the loop. ?>
        <?php do_action('generate_after_main_content'); ?>
    </main><!-- #main -->
    <script>

    jQuery(document).ready(function ($) {
        $('.column-2').each(function () {
            var currentAnalyst = $(this).text().replace(" ", "-").toLowerCase();
            $(this).wrapInner("<a href='http://192.168.33.10/aps/" + currentAnalyst + "-scorecard 'data-featherlight='iframe'></a>");
        });
    });

    </script>
</div><!-- #primary -->
<?php
// No direct access, please
if ( ! defined( 'ABSPATH' ) ) exit;
?>

	</div><!-- #content -->
</div><!-- #page -->

<!-- Boxes -->
<div class="boxes">
	<div class="box-row light">
		<div class="inner-box-row">
			<div class="box">
				<div class="inner-box">
					<div class="top5">Top 5</div>			
					<div class="title">Overall Score</div>
					<div class="list">
						<ol>
							<li><?php echo the_field('overall_expert_1') ?><span><?php echo the_field('overall_score_1') ?></span></li>
							<li><?php echo the_field('overall_expert_2') ?><span><?php echo the_field('overall_score_2') ?></span></li>
							<li><?php echo the_field('overall_expert_3') ?><span><?php echo the_field('overall_score_3') ?></span></li>
							<li><?php echo the_field('overall_expert_4') ?><span><?php echo the_field('overall_score_4') ?></span></li>
							<li><?php echo the_field('overall_expert_5') ?><span><?php echo the_field('overall_score_5') ?></span></li>
						</ol>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="inner-box">
					<div class="top5">Top 5</div>			
					<div class="title">Number of Current Picks</div>
					<div class="list">
						<ol>
							<li>James Davis <span>80</span></li>
							<li>Melanie Michelle Benton <span>79</span></li>
							<li>Elliot Sneeringer <span>78</span></li>
							<li>Melanie Michelle <span>77</span></li>
							<li>James Michael Davis <span>76</span></li>
						</ol>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="box-row dark">
		<div class="inner-box-row">
			<div class="box">
				<div class="inner-box">
					<div class="top5">Top 5</div>			
					<div class="title">Total Error (Least)</div>
					<div class="list">
						<ol>
							<li>James Davis <span>80</span></li>
							<li>Melanie Michelle Benton <span>79</span></li>
							<li>Elliot Sneeringer <span>78</span></li>
							<li>Melanie Michelle <span>77</span></li>
							<li>James Michael Davis <span>76</span></li>
						</ol>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="inner-box">
					<div class="top5">Top 5</div>			
					<div class="title">Average Mock</div>
					<div class="list">
						<ol>
							<li>James Davis <span>80</span></li>
							<li>Melanie Michelle Benton <span>79</span></li>
							<li>Elliot Sneeringer <span>78</span></li>
							<li>Melanie Michelle <span>77</span></li>
							<li>James Michael Davis <span>76</span></li>
						</ol>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="box-row light">
		<div class="inner-box-row">
			 <div class="box">
				<div class="inner-box">
					<div class="top5">Top 5</div>			
					<div class="title">Difficulty Points</div>
					<div class="list">
						<ol>
							<li>James Davis <span>80</span></li>
							<li>Melanie Michelle Benton <span>79</span></li>
							<li>Elliot Sneeringer <span>78</span></li>
							<li>Melanie Michelle <span>77</span></li>
							<li>James Michael Davis <span>76</span></li>
						</ol>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="inner-box">
					<div class="top5">Top 5</div>			
					<div class="title">Team Matching</div>
					<div class="list">
						<ol>
							<li>James Davis <span>80</span></li>
							<li>Melanie Michelle Benton <span>79</span></li>
							<li>Elliot Sneeringer <span>78</span></li>
							<li>Melanie Michelle <span>77</span></li>
							<li>James Michael Davis <span>76</span></li>
						</ol>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<!-- Custom Footer -->
<footer>
	<div class="inner-footer">
		<div class="left">
			<div class="inner-left">
				<img src="http://192.168.33.10/aps/wp-content/uploads/2017/11/aps_logo_white.png" class="logo" alt="Logo"/>
				<?php wp_nav_menu(); ?>
				<div class="copyright">&copy 2018 AccuProSports®, LLC</div>
			</div>
		</div>
		<div class="right">
			<div class="inner-right">
				<div class="social">
					<span>Follow Us</span>
					<a href="" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					<a href="" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a href="" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>	
				</div>
				<form class="email-footer">
					<div class="inner-form">
						<div class="input">
							<input class="email-input" placeholder="Your email address" type="text"/>
						</div>
						<div class="submit">
							<input class="submit-btn" type="submit" value="Subscribe" />
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
				<div class="form-text">
					Be the first to get the latest updated mocks
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>