<?php
/**
 * Template Name: APS Mock Modal Template
 *
 * @package GeneratePress
 */

// No direct access, please
if ( ! defined( 'ABSPATH' ) ) exit;
get_header('modal'); ?>

<div id="primary" <?php generate_content_class();?>>
    <main id="main" <?php generate_main_class(); ?>>
        <?php do_action('generate_before_main_content'); ?>
        <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'content', 'mock' ); ?>

        <?php endwhile; // end of the loop. ?>
        <?php do_action('generate_after_main_content'); ?>
    </main><!-- #main -->
    <style>
        .tablepress-id-2 .column-1,
        .tablepress-id-2 .column-4{
            text-align: center;
        }

        .tablepress-id-2 .column-1{
            border-left:none;
        }
        
        .tablepress-id-2 .column-5{
            border-right:none;
        }

        .tablepress-id-2 th,
        .tablepress-id-2 td {
            border: 1px solid #cccccc;
        }


    </style>
</div><!-- #primary -->
<?php
get_footer('modal');

