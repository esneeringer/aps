<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <main id="main">
 *
 * @package GeneratePress
 */
 
// No direct access, please
if ( ! defined( 'ABSPATH' ) ) exit;

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700,800" rel="stylesheet">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<style>
	.countdown-box{
		display: none;
	}
</style>

<body <?php generate_body_schema();?> <?php body_class(); ?>>
	<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'generatepress' ); ?>"><?php _e( 'Skip to content', 'generatepress' ); ?></a>
	<?php do_action( 'generate_before_header' ); ?>
	<?php do_action( 'generate_header' ); ?>
	<?php do_action( 'generate_after_header' ); ?>

	<!-- Header Image -->
	<!-- TO-DO: Depending on Page, change header Image -->
	<div class="header-image">
		<img class="shadow" src="/aps/wp-content/uploads/2017/11/Screen-Shot-2017-11-15-at-8.44.45-PM.png">
		<div class="overlay">
			<h3>2018 <span>NFL Draft</span></h3>
			<h1>Mock Drafts</h1>
			<p>Statement describing the following table of the mock drafts. This will be super informative and will not exceed three lines of text. There will be 37 Experts total.</p>
		</div>
	</div>

	<!--Countdown Clock -->
	<div class="countdown-box">	
		<div class="inner-countdown-box">
			<div class="countdown-text">
				<div class="inner-countdown-text">
					<b>2018 NFL Draft</b> Countdown
				</div>
			</div>
			<div class="countdown">
				<div class="inner-countdown">
					<span class="days">92</span><span class="hours">21</span><span class="minutes">11</span><span class="seconds">41</span>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>

	<div id="page" class="hfeed site grid-container container grid-parent">
		<div id="content" class="site-content">
			<?php do_action( 'generate_inside_container' ); ?>