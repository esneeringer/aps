<?php
/**
 * Template Name: APS Grades Footer 
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package GeneratePress
 */
 
// No direct access, please
if ( ! defined( 'ABSPATH' ) ) exit;
?>

	</div><!-- #content -->
</div><!-- #page -->

<!-- Boxes -->
<div class="boxes">
	<div class="box-row light">
		<div class="inner-box-row">
			<div class="box">
				<div class="inner-box">
					<div class="top5">Top 5</div>			
					<div class="title">Overall Score</div>
					<div class="list">
						<ol>
							<li><?php var_dump(the_field('overall_score_1')) ?><span>80</span></li>
							<li>Melanie Michelle Benton <span>79</span></li>
							<li>Elliot Sneeringer <span>78</span></li>
							<li>Melanie Michelle <span>77</span></li>
							<li>James Michael Davis <span>76</span></li>
						</ol>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="inner-box">
					<div class="top5">Top 5</div>			
					<div class="title">Number of Current Picks</div>
					<div class="list">
						<ol>
							<li>James Davis <span>80</span></li>
							<li>Melanie Michelle Benton <span>79</span></li>
							<li>Elliot Sneeringer <span>78</span></li>
							<li>Melanie Michelle <span>77</span></li>
							<li>James Michael Davis <span>76</span></li>
						</ol>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="box-row dark">
		<div class="inner-box-row">
			<div class="box">
				<div class="inner-box">
					<div class="top5">Top 5</div>			
					<div class="title">Total Error (Least)</div>
					<div class="list">
						<ol>
							<li>James Davis <span>80</span></li>
							<li>Melanie Michelle Benton <span>79</span></li>
							<li>Elliot Sneeringer <span>78</span></li>
							<li>Melanie Michelle <span>77</span></li>
							<li>James Michael Davis <span>76</span></li>
						</ol>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="inner-box">
					<div class="top5">Top 5</div>			
					<div class="title">Average Mock</div>
					<div class="list">
						<ol>
							<li>James Davis <span>80</span></li>
							<li>Melanie Michelle Benton <span>79</span></li>
							<li>Elliot Sneeringer <span>78</span></li>
							<li>Melanie Michelle <span>77</span></li>
							<li>James Michael Davis <span>76</span></li>
						</ol>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="box-row light">
		<div class="inner-box-row">
			 <div class="box">
				<div class="inner-box">
					<div class="top5">Top 5</div>			
					<div class="title">Difficulty Points</div>
					<div class="list">
						<ol>
							<li>James Davis <span>80</span></li>
							<li>Melanie Michelle Benton <span>79</span></li>
							<li>Elliot Sneeringer <span>78</span></li>
							<li>Melanie Michelle <span>77</span></li>
							<li>James Michael Davis <span>76</span></li>
						</ol>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="inner-box">
					<div class="top5">Top 5</div>			
					<div class="title">Team Matching</div>
					<div class="list">
						<ol>
							<li>James Davis <span>80</span></li>
							<li>Melanie Michelle Benton <span>79</span></li>
							<li>Elliot Sneeringer <span>78</span></li>
							<li>Melanie Michelle <span>77</span></li>
							<li>James Michael Davis <span>76</span></li>
						</ol>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<!-- Custom Footer -->
<footer>
	<div class="inner-footer">
		<div class="left">
			<div class="inner-left">
				<img src="http://192.168.33.10/aps/wp-content/uploads/2017/11/aps_logo_white.png" class="logo" alt="Logo"/>
				<?php wp_nav_menu(); ?>
				<div class="copyright">&copy 2018 AccuProSports®, LLC</div>
			</div>
		</div>
		<div class="right">
			<div class="inner-right">
				<div class="social">
					<span>Follow Us</span>
					<a href="" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					<a href="" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					<a href="" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>	
				</div>
				<form class="email-footer">
					<div class="inner-form">
						<div class="input">
							<input class="email-input" placeholder="Your email address" type="text"/>
						</div>
						<div class="submit">
							<input class="submit-btn" type="submit" value="Subscribe" />
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
				<div class="form-text">
					Be the first to get the latest updated mocks
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>