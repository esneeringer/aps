<?php
/**
 * Template Name: APS Mock Draft Main Template
 *
 * @package GeneratePress
 */

// No direct access, please
if ( ! defined( 'ABSPATH' ) ) exit;
get_header('mock'); ?>
<style>
    .tablepress-id-1 th,
    .tablepress-id-1 td {
        border: 1px solid #cccccc;
    }

    .tablepress-id-1 .column-1{
        width: 15%;
        border-left: none;
    }

    .tablepress-id-1 .column-2 {
        text-align: center;
        width: 12%;
    }

    .tablepress-id-1 .column-3 {
        text-align: center;
        width: 5%;
    }

    .tablepress-id-1 .column-4{
        width: 15%;
    }

    .tablepress-id-1 .column-5{
        width: 15%;
        border-right: none;
    }

    .tablepress-id-1{
        padding-left: 50px;
    }
</style>
<div id="primary" <?php generate_content_class();?>>
<main id="main" <?php generate_main_class(); ?>>
    <?php do_action('generate_before_main_content'); ?>
    <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'content', 'page' ); ?>

        <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || '0' != get_comments_number() ) : ?>
            <div class="comments-area">
                <?php comments_template(); ?>
            </div>
        <?php endif; ?>

    <?php endwhile; // end of the loop. ?>
    <?php do_action('generate_after_main_content'); ?>
</main><!-- #main -->
<script>

jQuery(document).ready(function ($) {
    $('.column-1').each(function () {
        var currentAnalyst = $(this).text().replace(" ", "-").toLowerCase();
        $(this).wrapInner("<a href='http://192.168.33.10/aps/" + currentAnalyst + "-mock 'data-featherlight='iframe'></a>");
    });

    </script>
</div><!-- #primary -->
<?php
get_footer();

